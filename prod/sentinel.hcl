

#policy "ec2-restrict-instance-type" {
#   enforcement_level = "soft-mandatory"
#}

policy "s3-server-side-encryption" {
    enforcement_level = "hard-mandatory"
}