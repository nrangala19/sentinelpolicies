# variable "tfe_token" {}

# variable "tfe_hostname" {
#   description = "The domain where your TFE is hosted."
#   default     = "app.terraform.io"
# }


variable "t_organization" {
  description = "The TFE organization to apply your changes to."
}

variable "t_workspace" {
  description = "The TFE organization to apply your changes to."
}

variable "t_policies_path" {
  description = "The TFE organization to apply your changes to."
}

variable "t_vcs_identifier" {
  description = "The TFE organization to apply your changes to."
}

variable "t_oauth_token_id" {
  description = "The TFE organization to apply your changes to."
}

# terraform {
#   backend "remote" {
#     hostname     = "app.terraform.io"
#     # organization = "hashicorp-v2"
#   }
# }

provider "tfe" {
  # hostname = "app.terraform.io"
  token    = "CYJz7m6ZZgDzmQ.atlasv1.gcWYqb5cN9qUi7akFjUs9guiFb2oyptgZdfXEZyEQhh7eOuR4mT8qEhU7mKuJzMLezw"
  version  = "~> 0.6"
}

data "tfe_workspace_ids" "all" {
  names        = ["*"]
  organization = "${var.t_organization}"
}

locals {
  # Use a shorter name for this map in policy set resources:
  workspaces = "${data.tfe_workspace_ids.all.external_ids}"
}

# 

resource "tfe_policy_set" "baseline" {
  name                   = "${var.t_workspace}-baseline"
  description            = "A brand new policy set"
  organization           = "${var.t_organization}"
  policies_path          = "${var.t_policies_path}"
  workspace_external_ids =  ["${local.workspaces["${var.t_workspace}"]}",]

  vcs_repo {
    identifier         = "${var.t_vcs_identifier}"
    branch             = "master"
    ingress_submodules = false
    oauth_token_id     = "${var.t_oauth_token_id}"
  }
}


# resource "tfe_policy_set" "baseline" {
#   name                   = "${var.t_workspace}-baseline"
#   description            = "A brand new policy set"
#   organization           = "${var.t_organization}"
#   policies_path          = "${var.t_policies_path}"
#   workspace_external_ids =  ["${local.workspaces["${var.t_workspace}"]}",]

#   vcs_repo {
#     identifier         = "${var.t_vcs_identifier}"
#     branch             = "master"
#     ingress_submodules = false
#     oauth_token_id     = "${var.t_oauth_token_id}"
#   }
# }