variable "tfe_organization" {
  description = "The TFE organization to apply your changes to."
}
variable "tfe_vcs_identifier" {
  description = "The TFE organization to apply your changes to."
}

variable "tfe_oauth_token_id" {
  description = "The TFE organization to apply your changes to."
}


variable "tfe_workspace_prod" {
  description = "The TFE workspace to apply your changes to."
}

variable "tfe_policies_path_prod" {
  description = "The TFE workspace to apply your changes to."
}


# variable "tfe_workspace_non_prod" {
#   description = "The TFE workspace to apply your changes to."
# }

# variable "tfe_policies_path_non_prod" {
#   description = "The TFE workspace to apply your changes to."
# }


# variable "tfe_workspace_sandbox" {
#   description = "The TFE workspace to apply your changes to."
# }

# variable "tfe_policies_path_sandbox" {
#   description = "The TFE workspace to apply your changes to."
# }

# variable "tfe_workspace_core" {
#   description = "The TFE workspace to apply your changes to."
# }

# variable "tfe_policies_path_core" {
#   description = "The TFE workspace to apply your changes to."
# }


# provider "tfe" {
#   version  = "~> 0.6"
# }

module "sentinel_policies_prod" {
  source = "./prod"
  t_organization = "${var.tfe_organization}"
  t_workspace = "${var.tfe_workspace_prod}"
  t_policies_path = "${var.tfe_policies_path_prod}"
  t_vcs_identifier = "${var.tfe_vcs_identifier}"
  t_oauth_token_id = "${var.tfe_oauth_token_id}"
}

# module "sentinel_policies_non_prod" {
#   source = "./non_prod"
#   t_organization = "${var.tfe_organization}"
#   t_workspace = "${var.tfe_workspace_non_prod}"
#   t_policies_path = "${var.tfe_policies_path_non_prod}"
#   t_vcs_identifier = "${var.tfe_vcs_identifier}"
#   t_oauth_token_id = "${var.tfe_oauth_token_id}"
# }
